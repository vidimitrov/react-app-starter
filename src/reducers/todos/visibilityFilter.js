import {SET_VISIBILITY_FILTER} from '../../constants/actionTypes';
import {
  VisibilityFilters,
} from '../../constants/visibilityFilters';
const {SHOW_ALL} = VisibilityFilters;

/**
 * Separated visibility filter actions
 * for reducer composition
 * @param {string} state - array of todos
 * @param {object} action
 * @return {array}
 */
export default function visibilityFilter(state = SHOW_ALL, action) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
}

