// Reducers
import {todos, visibilityFilter} from './todos';

// Combine them all and return them as a single reducer
export {
  todos,
  visibilityFilter,
};

